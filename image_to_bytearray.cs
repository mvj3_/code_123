            Uri uri = new Uri("ms-appx:///Assets/logo.png");
            RandomAccessStreamReference streamRef = RandomAccessStreamReference.CreateFromUri(uri);

            // Create a buffer for reading the stream 
            Windows.Storage.Streams.Buffer buffer = null;

            // Read the entire file into buffer
            using (IRandomAccessStreamWithContentType fileStream = await streamRef.OpenReadAsync())
            {
                buffer = new Windows.Storage.Streams.Buffer((uint)fileStream.Size);
                await fileStream.ReadAsync(buffer, (uint)fileStream.Size, InputStreamOptions.None);
            }